import React, {Component} from 'react'

class TodoItem extends Component {
  constructor (props) {
    super(props)

    this.handlerItem = this.handlerItem.bind(this)
  }

  render () {
    const { content } = this.props
    return (
      <li onClick={this.handlerItem}>{content}</li>
    )
  }

  handlerItem () {
    const {deleteItem, index} = this.props
    deleteItem(index)
    // this.props.deleteItem(this.props.index)
  }
}

export default TodoItem
