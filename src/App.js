import React, { Component } from 'react'
import TodoList from './todoList.js'

class App extends Component {
  render () {
    return (
      <div className="App">
        <TodoList></TodoList>
      </div>
    )
  }
}

export default App;
