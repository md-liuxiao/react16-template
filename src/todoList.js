import React, { Component, Fragment } from 'react'
import TodoItem from './todoItem.js'

import './todoList.css'

class TodoList extends Component {
  constructor (props) {
    super(props)
    
    this.state = {
      inputValue: '',
      list: []
    }

    this.handlerInputValueChange = this.handlerInputValueChange.bind(this)
    this.appendList = this.appendList.bind(this)
  }

  render () {
    return (
      <Fragment>
        <label htmlFor="input">输入内容</label>
        <input
          id="input"
          className="input"
          value={this.state.inputValue}
          onChange={this.handlerInputValueChange}
          onKeyUp={this.appendList}>
        </input>
        {/* <button onClick={this.appendList.bind(this)}>提交</button> */}
        <ul>
            {
              this.state.list.map((item, index) => {
                return (
                  <div key={index}>
                    <TodoItem
                      content={item}
                      index={index}
                      deleteItem={this.deleteListItem.bind(this, index)}/>
                  </div>
                  // <li
                  //   key={index}
                  //   onClick={this.deleteListItem.bind(this, index)}
                  //   // 危险转义
                  //   dangerouslySetInnerHTML={{__html: item}}>
                  //     {/* {item} */}
                  // </li>
                )
              })
            }
        </ul>
      </Fragment>
    )
  }

  handlerInputValueChange (e) {
    const value = e.target.value

    this.setState(() => {
      return {
        inputValue: value
      }
    })
    // this.setState({
    //   inputValue: e.target.value
    // })
  }

  appendList (e) {
    if (!this.state.inputValue) {
      return
    }

    if (e.keyCode === 13) {
      this.setState((prevState) => {
        return {
          list: [...prevState.list, prevState.inputValue],
          inputValue: ''
        }
      })
      // this.setState({
      //   list: [...this.state.list, this.state.inputValue],
      //   inputValue: ''
      // })
    }
  }

  deleteListItem (index, e) {
    
    this.setState((prevState) => {
      let newList = [...prevState.list]
  
      newList.splice(index, 1)
      return {
        list: newList
      }
    })
    // this.setState({
    //   list: newList
    // })
  }
}

export default TodoList
